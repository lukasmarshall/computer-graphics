package ass2.spec;

import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

/**
 * COMMENT: Comment Tree 
 *
 * @author malcolmr
 */
public class Tree {

    private double[] myPos;
    
    public Tree(double x, double y, double z) {
        myPos = new double[3];
        myPos[0] = x;
        myPos[2] = y;
        myPos[2] = z;
    }
    
    public double[] getPosition() {
        return myPos;
    }
    
    public void draw(GL2 gl, Terrain t, float widthScale, int[] texture){
	// Set camera.
	//        setCamera(gl, glu, 30);
		GLU glu = new GLU();
	    
	    
	    
	 // Draw cylinder
	final float cylinderRadius = 0.05f;
	final float cylinderHeight = 1f;
	final int cylinderSlices = 2;
	int slices = 16;
	int stacks = 16;
	float transX = widthScale * (float)myPos[0];
	float transY = widthScale * (float)myPos[2];
	//        float transZ = 0.0f;
	float transZ = (float) ( t.altitude(myPos[0] * widthScale, myPos[2] * widthScale) );
	

	//translate to tree base position
	gl.glTranslatef(transX,transY ,transZ);
	
	//texture mapping
	gl.glBindTexture(GL2.GL_TEXTURE_2D, texture[3]);
	
	GLUquadric body = glu.gluNewQuadric();
	glu.gluQuadricTexture(body, true);
	
	glu.gluQuadricDrawStyle(body, GLU.GLU_FILL);
	glu.gluQuadricNormals(body, GLU.GLU_FLAT);
	glu.gluQuadricOrientation(body, GLU.GLU_OUTSIDE);   
	glu.gluDisk(body, 0, cylinderRadius, cylinderSlices, 2);
	glu.gluCylinder(body, cylinderRadius, cylinderRadius, cylinderHeight, slices, stacks);
	gl.glTranslatef(0, 0, cylinderHeight);
	glu.gluDisk(body, 0, cylinderRadius, cylinderSlices, 2);
	glu.gluDeleteQuadric(body);
	    
	 // Draw sphere (possible styles: FILL, LINE, POINT).
	gl.glColor3f(0.3f, 0.5f, 1f);
	gl.glBindTexture(GL2.GL_TEXTURE_2D, texture[4]);
	
	GLUquadric sphere = glu.gluNewQuadric();
	glu.gluQuadricTexture(sphere, true);
	glu.gluQuadricDrawStyle(sphere, GLU.GLU_FILL);
	glu.gluQuadricNormals(sphere, GLU.GLU_FLAT);
	glu.gluQuadricOrientation(sphere, GLU.GLU_OUTSIDE);
	final float radius = 0.3f;
	glu.gluSphere(sphere, radius, slices, stacks);
	glu.gluDeleteQuadric(sphere);
	gl.glTranslatef(0, 0, -cylinderHeight);
	 
	//translate back to origin
	gl.glTranslatef(-transX,-transY ,-transZ);   
    }
    

}
