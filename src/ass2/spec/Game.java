package ass2.spec;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLJPanel;
import javax.media.opengl.glu.GLU;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;



/**
 * COMMENT: Comment Game 
 *
 * @author malcolmr
 */
public class Game extends JFrame implements GLEventListener{

    private Terrain myTerrain;

    //added by luke.
    private GLU glu = new GLU();
    private float rQuad = 0.0f, rTri = 0.0f;
    
    private int[] texture = new int[7];
    
//    private float xRotation = -45.0f;
    private float xRotation = -90.0f;
	private float yRotation = 0.0f;
	private float zRotation = 45.0f;
	
	private float[] lightAmbient = {10.5f, 10.5f, 10.5f, 1.0f};
	private float[] lightDiffuse = {1.0f, 1.0f, 1.0f, 1.0f};
	private float[] lightPosition = {0.0f, 0.0f, 2.0f, 1.0f};
	
//	private float[] cameraPosition = {-5.0f, -5.0f, -15.0f};
	private float[] cameraPosition = {0f, 0f, -1.0f};
	private float cameraHeight = 1.25f;
	
	private int filter;
	private float xSpeed, ySpeed;
	private boolean light;
	private boolean blend;
	private boolean avatar;
    
    public Game(Terrain terrain) {
    	super("Assignment 2");
        myTerrain = terrain;
        
   
    }
    
    /** 
     * Run the game.
     *
     */
    public void run() {
    	  GLProfile glp = GLProfile.getDefault();
          GLCapabilities caps = new GLCapabilities(glp);
          GLJPanel panel = new GLJPanel();
          panel.addGLEventListener(this);
 
          // Add an animator to call 'display' at 60fps        
          FPSAnimator animator = new FPSAnimator(60);
          animator.add(panel);
          animator.start();

          getContentPane().add(panel);
          setSize(800, 600);
          this.keyBindings(panel, this, this);
          setVisible(true);
          setDefaultCloseOperation(EXIT_ON_CLOSE);   
    }
    
    /**
     * Load a level file and display it.
     * 
     * @param args - The first argument is a level file in JSON format
     * @throws FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
        Terrain terrain = LevelIO.load(new File(args[0]));
        Game game = new Game(terrain);
        game.run();
    }

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
		gl.glLoadIdentity();
		gl.glMatrixMode(GL2.GL_MODELVIEW);

		gl.glRotatef(xRotation, 1.0f,0.0f, 0.0f);
		gl.glRotatef(yRotation, 0.0f,1.0f, 0.0f);
		gl.glRotatef(zRotation, 0.0f,0.0f, 1.0f);
		gl.glTranslatef(cameraPosition[0], cameraPosition[1], cameraPosition[2]);
	
		gl.glBindTexture(GL2.GL_TEXTURE_2D, texture[filter]);
		
		if(light)
			gl.glEnable(GL2.GL_LIGHTING);
		else
			gl.glDisable(GL2.GL_LIGHTING);
		
		if(blend){
			gl.glEnable(GL2.GL_BLEND);
			gl.glDisable(GL2.GL_DEPTH_TEST);
		}else{
			gl.glDisable(GL2.GL_BLEND);
			gl.glEnable(GL2.GL_DEPTH_TEST);
			
		}
		
		
		//Draw sunlight vector for debugging purposes.
//		gl.glBegin(GL2.GL_LINES);
//		gl.glVertex3f(this.lightPosition[0], this.lightPosition[1], this.lightPosition[2]);
//		gl.glVertex3f(0, 0, 0);
//		gl.glEnd();
		
		
		this.myTerrain.draw(gl, texture);
//		if(avatar)
//			drawAvatar(gl);
		
		gl.glFlush(); 
		
		
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub
		final GL2 gl = drawable.getGL().getGL2();
		gl.glShadeModel(GL2.GL_SMOOTH); //smooth lines
		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //set background color
		gl.glClearDepth(1.0f); //set depth functionality
		gl.glEnable(GL2.GL_DEPTH_TEST); //enable depth functionality
		gl.glDepthFunc(GL2.GL_LEQUAL);
		gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST); //makes it look pretty
		
		//Adjust position of sun.
		float [] sunPos = this.myTerrain.getSunlight();
		this.lightPosition[0] = sunPos[0];
		this.lightPosition[0] = sunPos[2];
		this.lightPosition[0] = -sunPos[1];
		
		gl.glEnable(GL2.GL_TEXTURE_2D); //enable textures.
		try{
			File  in = new File("./Assets/assignmentGrass.png");
			Texture t = TextureIO.newTexture(in, true);
			texture[0] = t.getTextureObject(gl);
			gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER,  GL2.GL_NEAREST);
			gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER,  GL2.GL_LINEAR_MIPMAP_NEAREST);
			gl.glGenerateMipmap(GL2.GL_TEXTURE_2D);
			gl.glBindTexture(GL2.GL_TEXTURE_2D, texture[0]);
			
			t = TextureIO.newTexture(in, true);
			texture[1] = t.getTextureObject(gl);
			gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER,  GL2.GL_LINEAR);
			gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER,  GL2.GL_LINEAR_MIPMAP_NEAREST);
			gl.glGenerateMipmap(GL2.GL_TEXTURE_2D);
			gl.glBindTexture(GL2.GL_TEXTURE_2D, texture[1]);
			
			t = TextureIO.newTexture(in, true);
			texture[2] = t.getTextureObject(gl);
			gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER,  GL2.GL_LINEAR);
			gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER,  GL2.GL_LINEAR_MIPMAP_NEAREST);
			gl.glGenerateMipmap(GL2.GL_TEXTURE_2D);
			gl.glBindTexture(GL2.GL_TEXTURE_2D, texture[2]);
			
			in = new File("./Assets/bark.png");
			t = TextureIO.newTexture(in, true);
			texture[3] = t.getTextureObject(gl);
			gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER,  GL2.GL_NEAREST);
			gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER,  GL2.GL_LINEAR_MIPMAP_NEAREST);
			gl.glGenerateMipmap(GL2.GL_TEXTURE_2D);
			gl.glBindTexture(GL2.GL_TEXTURE_2D, texture[3]);
			
			in = new File("./Assets/leaves.jpg");
			t = TextureIO.newTexture(in, true);
			texture[4] = t.getTextureObject(gl);
			gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER,  GL2.GL_NEAREST);
			gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER,  GL2.GL_LINEAR_MIPMAP_NEAREST);
			gl.glGenerateMipmap(GL2.GL_TEXTURE_2D);
			gl.glBindTexture(GL2.GL_TEXTURE_2D, texture[4]);
			
			in = new File("./Assets/road.jpg");
			t = TextureIO.newTexture(in, true);
			texture[5] = t.getTextureObject(gl);
			gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER,  GL2.GL_NEAREST);
			gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER,  GL2.GL_LINEAR_MIPMAP_NEAREST);
			gl.glGenerateMipmap(GL2.GL_TEXTURE_2D);
			gl.glBindTexture(GL2.GL_TEXTURE_2D, texture[5]);
			
			in = new File("./Assets/robot.jpg");
			t = TextureIO.newTexture(in, true);
			texture[6] = t.getTextureObject(gl);
			gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER,  GL2.GL_NEAREST);
			gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER,  GL2.GL_LINEAR_MIPMAP_NEAREST);
			gl.glGenerateMipmap(GL2.GL_TEXTURE_2D);
			gl.glBindTexture(GL2.GL_TEXTURE_2D, texture[6]);
			
			//Lighting
			gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_AMBIENT, this.lightAmbient,0);
			gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_DIFFUSE, this.lightDiffuse,0);
			gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_POSITION, this.lightPosition,0);
			gl.glEnable(GL2.GL_LIGHT1);
			gl.glEnable(GL2.GL_LIGHTING);
			
			this.light = true;
			gl.glColor4f(1.0f, 1.0f, 1.0f, 0.5f); //50% alpha
			gl.glBlendFunc(GL2.GL_SRC_ALPHA, GL2.GL_ONE);
			
			this.avatar = true;
			

		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void drawAvatar(GL2 gl){
		
		
		gl.glPushMatrix();
		gl.glLoadIdentity();
		gl.glBindTexture(GL2.GL_TEXTURE_2D, texture[6]);
		gl.glBegin(GL2.GL_TRIANGLES); 
		
		float cx = this.cameraPosition[0];
		float cy = this.cameraPosition[1];
		float cz = this.cameraPosition[2];
		
		float[] point1 = {1f ,0f,0f};
		float[]point2 = {-1f,-1f,1f};
		float[] point3 = {1f,-1f,1f};
		float[] point4 = {1.0f, -1.0f, -1.0f};
		float[] point5 = {-1.0f, -1.0f, -1.0f};
		float[] point6 = {-1.0f, -1.0f, 1.0f};
		drawTriangle(gl, point1, point2, point3);
		drawTriangle(gl, point1, point3, point4);
		drawTriangle(gl, point1, point4, point5);
		drawTriangle(gl, point1, point5, point6);
		gl.glEnd(); // Done Drawing The Pyramid
		gl.glPopMatrix();

		
		
		
	}
	
	private void drawTriangle(GL2 gl, float[] point1, float[] point2, float[] point3){

		gl.glTexCoord2f(0.0f, 0.0f); 
		gl.glVertex3f(point1[0], point1[1], point1[2]); // Top Of Triangle (Front)
		gl.glTexCoord2f(1.0f, 0.0f);
		gl.glVertex3f(point2[0], point2[1], point2[2]); // Top Of Triangle (Front)
		gl.glTexCoord2f(1.0f, 1.0f);
		gl.glVertex3f(point3[0], point3[1], point3[2]); // Top Of Triangle (Front)
		gl.glTexCoord2f(0.0f, 1.0f); 
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		// TODO Auto-generated method stub
		GL2 gl = drawable.getGL().getGL2();
		if(height <=0)
			height  = 1;
		float h = (float) width / (float) height;
		gl.glViewport(0, 0, width, height);
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluPerspective(45.0f, h, 1.0, 20.0);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
		
		
		
	}
	
	public static void keyBindings(JPanel p , final JFrame frame, final Game r){
		ActionMap actionMap = p.getActionMap();
		InputMap inputMap = p.getInputMap();
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F1,0), "F1");
		actionMap.put("F1", new AbstractAction(){
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent drawable){
//				fullscreen(frame);
				System.out.println("This command would usually fullscreen the app.");
			}
		});
		
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_W,0), "W");
		actionMap.put("W", new AbstractAction(){
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent drawable){
//				fullscreen(frame);
				System.out.println("W");
				r.xRotation -= 0.5f;
			}
		});
		
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_S,0), "S");
		actionMap.put("S", new AbstractAction(){
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent drawable){
//				fullscreen(frame);
				System.out.println("S");
				r.xRotation += 0.5f;
			}
		});
		
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT,0), "LEFT");
		actionMap.put("LEFT", new AbstractAction(){
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent drawable){
//				fullscreen(frame);
				System.out.println("Left");
				r.zRotation -= 3f;
			}
		});
		
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT,0), "RIGHT");
		actionMap.put("RIGHT", new AbstractAction(){
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent drawable){
//				fullscreen(frame);
				System.out.println("Right");
				r.zRotation += 3f;
			}
		});
		
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F,0), "F");
		actionMap.put("F", new AbstractAction(){
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent drawable){
//				fullscreen(frame);
				
				r.filter ++;
				if(r.filter > r.texture.length - 1)
					r.filter = 0;
				System.out.println("Filter: "+ r.filter);
			}
		});
		
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_L,0), "L");
		actionMap.put("L", new AbstractAction(){
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent drawable){
//				fullscreen(frame);
				
				if(r.light)
					r.light = false;
				else
					r.light = true;
				System.out.println("Lighting: "+r.light);
			}
		});
		
		
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_B,0), "B");
		actionMap.put("B", new AbstractAction(){
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent drawable){
//				fullscreen(frame);
				
				if(r.blend)
					r.blend = false;
				else
					r.blend = true;
				System.out.println("Blending: "+r.blend);
			}
		});
		
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP,0), "UP");
		actionMap.put("UP", new AbstractAction(){
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent drawable){

				
				r.cameraPosition[0] -= 0.5f * Math.sin(Math.toRadians(r.zRotation));
				r.cameraPosition[1] -= 0.5f * Math.cos(Math.toRadians(r.zRotation));
				float xPos = -1 * r.cameraPosition[0];
				float yPos = -1 * r.cameraPosition[1];
				if(xPos >= 0 && yPos >= 0){
					
					float altitude = (float)r.myTerrain.altitude((double) -r.cameraPosition[1], (double) -r.cameraPosition[0]);
					System.out.println("Calling the altitude brigade: " + altitude);
					r.cameraPosition[2] = -1.0f * (altitude + r.cameraHeight);
				}else{
					r.cameraPosition[2] = -1.0f * r.cameraHeight;
				}

				
 				System.out.println("UP: ("+r.cameraPosition[0]+" , "+r.cameraPosition[1] +" , "+r.cameraPosition[2]+")");
			}
		});
		
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN,0), "DOWN");
		actionMap.put("DOWN", new AbstractAction(){
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent drawable){
				
				r.cameraPosition[0] += 0.5f * Math.sin(Math.toRadians(r.zRotation));
				r.cameraPosition[1] += 0.5f * Math.cos(Math.toRadians(r.zRotation));
				float xPos = -1 * r.cameraPosition[0];
				float yPos = -1 * r.cameraPosition[1];
				if(xPos >= 0 && yPos >= 0){
					float altitude = (float)r.myTerrain.altitude((double) -r.cameraPosition[1], (double) -r.cameraPosition[0]);
					System.out.println("Calling the altitude brigade: "+altitude);
					r.cameraPosition[2] = -1.0f * (altitude + r.cameraHeight);
				}else{
					r.cameraPosition[2] = -1.0f * r.cameraHeight;
				}
 				System.out.println("DOWN: ("+r.cameraPosition[0]+" , "+r.cameraPosition[1]+" , "+r.cameraPosition[2]+")");
			}
		});
		
		
		
	}
}
