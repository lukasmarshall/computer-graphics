package ass2.spec;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL2;

/**
 * COMMENT: Comment Road 
 *
 * @author malcolmr
 */
public class Road {

    private List<Double> myPoints;
    private double myWidth;
    
    /** 
     * Create a new road starting at the specified point
     */
    public Road(double width, double x0, double y0) {
        myWidth = width;
        myPoints = new ArrayList<Double>();
        myPoints.add(x0);
        myPoints.add(y0);
    }

    /**
     * Create a new road with the specified spine 
     *
     * @param width
     * @param spine
     */
    public Road(double width, double[] spine) {
        myWidth = width;
        myPoints = new ArrayList<Double>();
        for (int i = 0; i < spine.length; i++) {
            myPoints.add(spine[i]);
        }
    }

    /**
     * The width of the road.
     * 
     * @return
     */
    public double width() {
        return myWidth;
    }

    /**
     * Add a new segment of road, beginning at the last point added and ending at (x3, y3).
     * (x1, y1) and (x2, y2) are interpolated as bezier control points.
     * 
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @param x3
     * @param y3
     */
    public void addSegment(double x1, double y1, double x2, double y2, double x3, double y3) {
        myPoints.add(x1);
        myPoints.add(y1);
        myPoints.add(x2);
        myPoints.add(y2);
        myPoints.add(x3);
        myPoints.add(y3);        
    }
    
    /**
     * Get the number of segments in the curve
     * 
     * @return
     */
    public int size() {
        return myPoints.size() / 6;
    }

    /**
     * Get the specified control point.
     * 
     * @param i
     * @return
     */
    public double[] controlPoint(int i) {
        double[] p = new double[2];
        p[0] = myPoints.get(i*2);
        p[1] = myPoints.get(i*2+1);
        return p;
    }
    
    /**
     * Get a point on the spine. The parameter t may vary from 0 to size().
     * Points on the kth segment take have parameters in the range (k, k+1).
     * 
     * @param t
     * @return
     */
    public double[] point(double t) {
        int i = (int)Math.floor(t);
        t = t - i;
        
        i *= 6;
        
        double x0 = myPoints.get(i++);
        double y0 = myPoints.get(i++);
        double x1 = myPoints.get(i++);
        double y1 = myPoints.get(i++);
        double x2 = myPoints.get(i++);
        double y2 = myPoints.get(i++);
        double x3 = myPoints.get(i++);
        double y3 = myPoints.get(i++);
        
        double[] p = new double[2];

        p[0] = b(0, t) * x0 + b(1, t) * x1 + b(2, t) * x2 + b(3, t) * x3;
        p[1] = b(0, t) * y0 + b(1, t) * y1 + b(2, t) * y2 + b(3, t) * y3;        
        
        return p;
    }
    
    /**
     * Calculate the Bezier coefficients
     * 
     * @param i
     * @param t
     * @return
     */
    private double b(int i, double t) {
        
        switch(i) {
        
        case 0:
            return (1-t) * (1-t) * (1-t);

        case 1:
            return 3 * (1-t) * (1-t) * t;
            
        case 2:
            return 3 * (1-t) * t * t;

        case 3:
            return t * t * t;
        }
        
        // this should never happen
        throw new IllegalArgumentException("" + i);
    }
    
    public void draw(GL2 gl, Terrain terrain, float widthScale, int[] texture){
    	//for every segment
    	//draw curve
    	
    	gl.glLineWidth(6f); 
    	gl.glColor3f(0.0f, 0.0f, 0.0f);
    	gl.glBindTexture(GL2.GL_TEXTURE_2D, texture[5]);
//    	gl.glBegin(GL2.GL_LINE_STRIP);
    	
    	
    	gl.glBegin(GL2.GL_TRIANGLES);
    	for(float t = 0; t<0.990;  t+= 0.001){
    		double[] point = this.point(t);
    		double[] point2 = this.point(t+0.01);
    		float altitude = (float) terrain.altitude(point[0], point[1]) + 0.01f;
//    		float altitude = 0;

    		//Finding the two points at a distance from point1.
    		double tangentGradient = (point2[1] - point[1]) / (point2[0] - point[0]);
    		double adjacentGradient = -1.0d / tangentGradient;
    		//where y = mx + b, m = adjacent gradient, y = point[1], x = point[2]
    		double b = point[1] - (adjacentGradient * point[0]);
    		double x3 = point[0] + (this.width() / 2.0f) / Math.sqrt(1 + (adjacentGradient * adjacentGradient));
    		double y3 = adjacentGradient * x3 + b;
    		double x4 = point[0] - (this.width() / 2.0f) / Math.sqrt(1 + (adjacentGradient * adjacentGradient));
    		double y4 = adjacentGradient * x4 + b;
    		
    		//Now moving to point 2:
    		b = point2[1] - (adjacentGradient * point2[0]);
    		double x5 = point2[0] + (this.width() / 2.0f) / Math.sqrt(1 + (adjacentGradient * adjacentGradient));
    		double y5 = adjacentGradient * x5 + b;
    		double x6 = point2[0] - (this.width() / 2.0f) / Math.sqrt(1 + (adjacentGradient * adjacentGradient));
    		double y6 = adjacentGradient * x6 + b;
    		
    		//triangle out one side from point 1
    		gl.glTexCoord2f(0.0f, 0.0f);
    		gl.glVertex3f((float)point[0], (float)point[1], altitude);
    		gl.glTexCoord2f(1.0f, 0.0f);
    		gl.glVertex3f((float)point2[0], (float)point2[1], altitude);
    		gl.glTexCoord2f(1.0f, 1.0f);
    		gl.glVertex3f((float)x3, (float)y3, altitude);
    		gl.glTexCoord2f(0.0f, 1.0f);
    		//triangle out the other from point 1
    		gl.glTexCoord2f(0.0f, 0.0f);
    		gl.glVertex3f((float)point[0], (float)point[1], altitude);
    		gl.glTexCoord2f(1.0f, 0.0f);
    		gl.glVertex3f((float)point2[0], (float)point2[1], altitude);
    		gl.glTexCoord2f(1.0f, 1.0f);
    		gl.glVertex3f((float)x4, (float)y4, altitude);
    		gl.glTexCoord2f(0.0f, 1.0f);
    		//triangle out one side from point 2
    		gl.glTexCoord2f(0.0f, 0.0f);
    		gl.glVertex3f((float)point2[0], (float)point2[1], altitude);
    		gl.glTexCoord2f(1.0f, 0.0f);
    		gl.glVertex3f((float)x4, (float)y4, altitude);
    		gl.glTexCoord2f(1.0f, 1.0f);
    		gl.glVertex3f((float)x6, (float)y6, (float)altitude);
    		gl.glTexCoord2f(0.0f, 1.0f);
    		//triangle out the other side from point 2
    		gl.glTexCoord2f(0.0f, 0.0f);
    		gl.glVertex3f((float)point2[0], (float)point2[1], altitude);
    		gl.glTexCoord2f(1.0f, 0.0f);
    		gl.glVertex3f((float)x3, (float)y3, altitude);
    		gl.glTexCoord2f(1.0f, 1.0f);
    		gl.glVertex3f((float)x5, (float)y5, (float)altitude);
    		gl.glTexCoord2f(0.0f, 1.0f);
    	}
    	gl.glEnd();
    }


}
