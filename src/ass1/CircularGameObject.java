package ass1;

import javax.media.opengl.GL2;

public class CircularGameObject extends GameObject {
	private double radius;
	private double[] fillColour;
	private double[] lineColour;
	
	//Create a CircularGameObject with centre 0,0 and radius 1
	public CircularGameObject(GameObject parent, double[] fillColour, double[] lineColour){
		super(parent);
		this.radius = 1.0;
		this.fillColour = fillColour.clone();
		this.lineColour = lineColour.clone();
	}

	//Create a CircularGameObject with centre 0,0 and a given radius
	public CircularGameObject(GameObject parent, double radius,double[] fillColour, double[] lineColour){
		super(parent);
		this.radius = radius;
		this.fillColour = fillColour.clone();
		this.lineColour = lineColour.clone();
	}
	
	//Had some help here from http://basic4gl.wikispaces.com/2D+Drawing+in+OpenGL
	@Override
    public void drawSelf(GL2 gl) {
		super.drawSelf(gl);
    	if(this.fillColour != null){
    		gl.glColor3f((float)this.fillColour[0], (float)this.fillColour[1], (float)this.fillColour[2]);
    		gl.glBegin(gl.GL_TRIANGLE_FAN);
    		gl.glVertex2f((float)super.getGlobalPosition()[0],(float) super.getGlobalPosition()[1]);
    		gl.glColor3f((float)this.fillColour[0], (float)this.fillColour[1], (float)this.fillColour[2]);
    		for (double i = 0; i< 365; i+= 5){
    			gl.glVertex2f((float)(super.getGlobalPosition()[0] + Math.sin(Math.toRadians(i)) * this.radius * this.getGlobalScale()), (float) (super.getGlobalPosition()[1] + Math.cos(Math.toRadians(i)) * this.radius * this.getGlobalScale()));
    		}
    		gl.glEnd();
    	}
    	
    	if(this.lineColour != null){
    		gl.glBegin(gl.GL_LINE_LOOP);
    		gl.glColor3f((float)this.lineColour[0], (float)this.lineColour[1], (float)this.lineColour[2]);
    		 for(int angle = 0; angle<365; angle+=5){
    			 gl.glVertex2f((float)(super.getGlobalPosition()[0] + Math.sin(Math.toRadians(angle)) * this.radius*this.getGlobalScale()), (float)(super.getGlobalPosition()[1] + Math.cos(Math.toRadians(angle)) * this.radius * this.getGlobalScale()));
    		 }
    		 gl.glEnd();
    	}

    }

}
