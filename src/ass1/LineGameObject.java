package ass1;

import javax.media.opengl.GL2;


public class LineGameObject extends GameObject {
	private float x1, x2, y1, y2;
	private double[] lineColour;
	
	//Create a LineGameObject from (0,0) to (1,0)
	public LineGameObject(GameObject parent, double[] lineColour){
		super(parent);
		this.x1 = 0;
		this.y1 = 0;
		this.x2 = 1;
		this.y2 = 0;
		this.lineColour = lineColour;
	}

	//Create a LineGameObject from (x1,y1) to (x2,y2)
	public LineGameObject(GameObject parent,  double x1, double y1, double x2, double y2, double[] lineColour){
		super(parent);
		this.x1 = (float) x1;
		this.x2 = (float) x2;
		this.y1 = (float) y1;
		this.y2 = (float) y2;
		this.lineColour = lineColour;
	}

	@Override
    public void drawSelf(GL2 gl) {
		super.drawSelf(gl);
    	if(this.lineColour != null){
    		gl.glBegin(gl.GL_LINES);
    		gl.glColor3f((float)this.lineColour[0], (float)this.lineColour[1], (float)this.lineColour[2]);
    		float[] point1 = this.localToAbsolute(this.x1, this.y1);
    		float[] point2 = this.localToAbsolute(this.x2, this.y2);
    		gl.glVertex2f(point1[0], point1[1]); 
    		gl.glVertex2f(point2[0], point2[1]);
    		gl.glEnd();
    	}

    }
	
	
	
	

}
