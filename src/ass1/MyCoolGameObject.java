package ass1;

import javax.media.opengl.GL2;

public class MyCoolGameObject extends GameObject{

	
	public MyCoolGameObject(){
		super(GameObject.ROOT);
		double[] yellow = {255,255,0,1};
		double[] black = {0,0,0,1};
		CircularGameObject face = new CircularGameObject(this,1,yellow,yellow );
		
		CircularGameObject rightEye = new CircularGameObject(this, 0.1, black, black);
		rightEye.setPosition(0.5, 0.4);
		CircularGameObject leftEye = new CircularGameObject(this, 0.1, black, black);
		leftEye.setPosition(-0.5, 0.4);
	}
	
	
	@Override
	public void drawSelf(GL2 gl){
		double radius = 0.5;
		
		float[] black = {0,0,0,1};
		float[] white = {1,1,1,1};
		
		
		gl.glColor3f(white[0], white[1], white[2]);
		gl.glBegin(gl.GL_TRIANGLE_FAN);
		gl.glVertex2f((float)super.getGlobalPosition()[0],(float) super.getGlobalPosition()[1]);
		gl.glColor3f(white[0], (float)white[1], white[2]);
		for (double i = 90 - this.getGlobalRotation(); i< 271- this.getGlobalRotation(); i+= 5){
			gl.glVertex2f((float)(super.getGlobalPosition()[0] + Math.sin(Math.toRadians(i)) * radius * this.getGlobalScale()), (float) (super.getGlobalPosition()[1] + Math.cos(Math.toRadians(i)) * radius * this.getGlobalScale()));
		}
		gl.glEnd();
		
		
		
		gl.glBegin(gl.GL_LINE_LOOP);
		gl.glColor3f(black[0], black[1], black[2]);
		 for(double angle = 90 - this.getGlobalRotation(); angle<271- this.getGlobalRotation(); angle+=1){
			 gl.glVertex2f((float)(super.getGlobalPosition()[0] + Math.sin(Math.toRadians(angle)) * radius*this.getGlobalScale()), (float)(super.getGlobalPosition()[1] + Math.cos(Math.toRadians(angle)) * radius * this.getGlobalScale()));
		 }
		 gl.glEnd();
	}

}
