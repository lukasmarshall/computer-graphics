package ass1;

import javax.media.opengl.GL2;

/**
 * The camera is a GameObject that can be moved, rotated and scaled like any other.
 * 
 * TODO: You need to implment the setView() and reshape() methods.
 *       The methods you need to complete are at the bottom of the class
 *
 * @author malcolmr
 */
public class Camera extends GameObject {

    private float[] myBackground;

    public Camera(GameObject parent) {
        super(parent);

        myBackground = new float[4];
    }

    public Camera() {
        this(GameObject.ROOT);
    }
    
    public float[] getBackground() {
        return myBackground;
    }

    public void setBackground(float[] background) {
        myBackground = background;
    }

    // ===========================================
    // COMPLETE THE METHODS BELOW
    // ===========================================
   
    
    //Help here from http://www.opengl.org/discussion_boards/showthread.php/128606-how-to-clear-screen-after-drawing
    public void setView(GL2 gl) {
        
        // TODO 1. clear the view to the background colour
        gl.glClearColor(myBackground[0], myBackground[1], myBackground[2], myBackground[3]);
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT);
        
        // TODO 2. set the view matrix to account for the camera's position  
        double[] position = super.getGlobalPosition();
        position[0] = -1.0 * position[0];
        position[1] = -1.0 * position[1];
        
        double[][] translation = MathUtil.translationMatrix(position);
        double[][] rotation = MathUtil.rotationMatrix(-1.0 * super.getGlobalRotation());
        double[][] scale = MathUtil.scaleMatrix(1.0 /super.getGlobalScale());
        
        gl.glMatrixMode(gl.GL_MODELVIEW);
        gl.glLoadIdentity();
        gl.glMultMatrixd(utilMatrixToConsecutive(scale), 0);
        gl.glMultMatrixd(utilMatrixToConsecutive(rotation), 0);
        gl.glMultMatrixd(utilMatrixToConsecutive(translation), 0);
        gl.glMatrixMode(gl.GL_PROJECTION);
        
    }

    public void reshape(GL2 gl, int x, int y, int width, int height) {
        // TODO  1. match the projection aspect ratio to the viewport
        // to avoid stretching
    	
    	gl.glMatrixMode(gl.GL_PROJECTION);
    	double ar = (double)width / (double)height;
    	gl.glViewport(x, y, width, height);
    	gl.glLoadIdentity();
    	gl.glOrtho(-1.0 * ar, 1.0 * ar, -1, 1, -1, 1);
    	gl.glMatrixMode(gl.GL_MODELVIEW);
    	gl.glLoadIdentity();
        
    }
    
    private double [] utilMatrixToConsecutive(double[][] original){
    	double [] flat = new double[16];
    	int k = 0;
    	for(int i = 0; i < 3; i++){
    		for(int j = 0; j < 3; j++){
    			flat[k] = original[i][j];
    			k ++;
    		}
    		flat[k] = 0;
    		k++;
    	}
    	flat[15] = 1;
    	return flat;
    }
}
