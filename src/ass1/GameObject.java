package ass1;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL2;


/**
 * A GameObject is an object that can move around in the game world.
 * 
 * GameObjects form a scene tree. The root of the tree is the special ROOT object.
 * 
 * Each GameObject is offset from its parent by a rotation, a translation and a scale factor. 
 *
 * TODO: The methods you need to complete are at the bottom of the class
 *
 * @author malcolmr
 */
public class GameObject {

    // the list of all GameObjects in the scene tree
    public final static List<GameObject> ALL_OBJECTS = new ArrayList<GameObject>();
    
    // the root of the scene tree
    public final static GameObject ROOT = new GameObject();
    
    // the links in the scene tree
    private GameObject myParent;
    private List<GameObject> myChildren;

    // the local transformation
    //myRotation should be normalised to the range (-180..180)
    private double myRotation;
    private double myScale;
    private double[] myTranslation;
    
    // is this part of the tree showing?
    private boolean amShowing;

    /**
     * Special private constructor for creating the root node. Do not use otherwise.
     */
    private GameObject() {
        myParent = null;
        myChildren = new ArrayList<GameObject>();

        myRotation = 0;
        myScale = 1;
        myTranslation = new double[2];
        myTranslation[0] = 0;
        myTranslation[1] = 0;

        amShowing = true;
        
        ALL_OBJECTS.add(this);
    }

    /**
     * Public constructor for creating GameObjects, connected to a parent (possibly the ROOT).
     *  
     * New objects are created at the same location, orientation and scale as the parent.
     *
     * @param parent
     */
    public GameObject(GameObject parent) {
        myParent = parent;
        myChildren = new ArrayList<GameObject>();

        parent.myChildren.add(this);

        myRotation = 0;
        myScale = 1;
        myTranslation = new double[2];
        myTranslation[0] = 0;
        myTranslation[1] = 0;

        // initially showing
        amShowing = true;

        ALL_OBJECTS.add(this);
    }

    /**
     * Remove an object and all its children from the scene tree.
     */
    public void destroy() {
        for (GameObject child : myChildren) {
            child.destroy();
        }
        
        myParent.myChildren.remove(this);
        ALL_OBJECTS.remove(this);
    }

    /**
     * Get the parent of this game object
     * 
     * @return
     */
    public GameObject getParent() {
        return myParent;
    }

    /**
     * Get the children of this object
     * 
     * @return
     */
    public List<GameObject> getChildren() {
        return myChildren;
    }

    /**
     * Get the local rotation (in degrees)
     * 
     * @return
     */
    public double getRotation() {
        return myRotation;
    }

    /**
     * Set the local rotation (in degrees)
     * 
     * @return
     */
    public void setRotation(double rotation) {
        myRotation = MathUtil.normaliseAngle(rotation);
    }

    /**
     * Rotate the object by the given angle (in degrees)
     * 
     * @param angle
     */
    public void rotate(double angle) {
        myRotation += angle;
        myRotation = MathUtil.normaliseAngle(myRotation);
    }

    /**
     * Get the local scale
     * 
     * @return
     */
    public double getScale() {
        return myScale;
    }

    /**
     * Set the local scale
     * 
     * @param scale
     */
    public void setScale(double scale) {
        myScale = scale;
    }

    /**
     * Multiply the scale of the object by the given factor
     * 
     * @param factor
     */
    public void scale(double factor) {
        myScale *= factor;
    }

    /**
     * Get the local position of the object 
     * 
     * @return
     */
    public double[] getPosition() {
        double[] t = new double[2];
        t[0] = myTranslation[0];
        t[1] = myTranslation[1];

        return t;
    }

    /**
     * Set the local position of the object
     * 
     * @param x
     * @param y
     */
    public void setPosition(double x, double y) {
        myTranslation[0] = x;
        myTranslation[1] = y;
    }

    /**
     * Move the object by the specified offset in local coordinates
     * 
     * @param dx
     * @param dy
     */
    public void translate(double dx, double dy) {
        myTranslation[0] += dx;
        myTranslation[1] += dy;
    }

    /**
     * Test if the object is visible
     * 
     * @return
     */
    public boolean isShowing() {
        return amShowing;
    }

    /**
     * Set the showing flag to make the object visible (true) or invisible (false).
     * This flag should also apply to all descendents of this object.
     * 
     * @param showing
     */
    public void show(boolean showing) {
        amShowing = showing;
    }

    /**
     * Update the object. This method is called once per frame. 
     * 
     * This does nothing in the base GameObject class. Override this in subclasses.
     * 
     * @param dt The amount of time since the last update (in seconds)
     */
    public void update(double dt) {
        // do nothing
    }

    /**
     * Draw the object (but not any descendants)
     * 
     * This does nothing in the base GameObject class. Override this in subclasses.
     * 
     * @param gl
     */
    public void drawSelf(GL2 gl) {
        // do nothing
    }

    
    // ===========================================
    // COMPLETE THE METHODS BELOW
    // ===========================================
    
    /**
     * Draw the object and all of its descendants recursively.
     * 
     * TODO: Complete this method
     * 1. Update the model transform appropriately to establish the local coordinate frame for this object. HMM
	 * 2. Call drawSelf() to draw the object. DONE
	 * 3. Call draw() recursively on all children objects DONE
     * 
     * @param gl
     */
    public void draw(GL2 gl) {
        
        // don't draw if it is not showing
        if (!amShowing) {
            return;
        }

        // TODO: draw the object and all its children recursively
        // setting the model transform appropriately 
        
        for (GameObject child : this.getChildren()){
        	child.draw(gl);
        }
        
        
        // Call drawSelf() to draw the object itself
        this.drawSelf(gl);
        
    }

    /**
     * Compute the object's position in world coordinates
     * 
     * TODO: Write this method DONE
     * Translate -> Rotate -> Scale
     * 
     * @return a point in world coordinates in [x,y] form
     */
    public double[] getGlobalPosition() {
        double[] p = new double[2];
        p = this.myTranslation.clone(); 
        
        if(this.myParent != null){
        	//Get the parent's model matrix.
	        double[][] modelMatrix = MathUtil.translationMatrix(this.myParent.getGlobalPosition());
	        modelMatrix = MathUtil.multiply(modelMatrix, MathUtil.rotationMatrix(this.myParent.getGlobalRotation()));
	        modelMatrix = MathUtil.multiply(modelMatrix, MathUtil.scaleMatrix(this.myParent.getGlobalScale()));
	        
	        //Multiply the position vector of the object by the model matrix of the parent.
	        double [] positionVector = new double[3];
	        positionVector[0] = this.myTranslation[0];
	        positionVector[1] = this.myTranslation[1];
	        positionVector[2] = 1;
	        positionVector = MathUtil.multiply(modelMatrix,  positionVector);
	        p[0] = positionVector[0];
	        p[1] = positionVector[1];
        }
        
        return p; 
    }
    
    

    /**
     * Compute the object's rotation in the global coordinate frame
     * 
     * TODO: Write this method DONE
     * 
     * @return the global rotation of the object (in degrees) and 
     * normalized to the range (-180, 180) degrees. 
     */
    public double getGlobalRotation() {
    	
    	double rotation = this.myRotation;
    	if(this.getParent() != null){
    		rotation = rotation + this.getParent().getGlobalRotation();
    	}
    	rotation = MathUtil.normaliseAngle(rotation);
        return rotation;
    }

    /**
     * Compute the object's scale in global terms
     * 
     * TODO: Write this method DONE
     * 
     * @return the global scale of the object 
     */
    public double getGlobalScale() {
    	double scale = this.myScale;
    	if(this.getParent() != null){
    		scale = this.getParent().getGlobalScale() * scale;
    	}
        return scale;
    }

    /**
     * Change the parent of a game object.
     * 
     * TODO: add code so that the object does not change its global position, rotation or scale
     * when it is reparented. 
     * 
     * @param parent
     */
    public void setParent(GameObject parent) {
    	double[] coords = new double [3];
    	coords[0] = this.getGlobalPosition()[0];
    	coords[1] = this.getGlobalPosition()[1];
    	coords[2] = 1;
    	
    	this.myScale = this.getGlobalScale() / parent.getGlobalScale();
    	this.myRotation = this.getGlobalRotation() - parent.getGlobalRotation();
    	this.myRotation = MathUtil.normaliseAngle(this.myRotation);
    	
    	//Theory here is we undo all the moves that bring us to the new parent, then apply them to the current global coords.
    	//The resulting vector should be the required transform.
    	
    	double[] reverseCoords = new double[3];
    	reverseCoords[0] = parent.getGlobalPosition()[0] * -1.0;
    	reverseCoords[1] = parent.getGlobalPosition()[1] * -1.0;
    	reverseCoords[2] = 1;
    	
    	//reverse way
    	double[][] reverseParentModel = MathUtil.scaleMatrix(1.0 / parent.getGlobalScale());
    	reverseParentModel = MathUtil.multiply(reverseParentModel, MathUtil.rotationMatrix(-1.0 * parent.getGlobalRotation()));
    	reverseParentModel = MathUtil.multiply(reverseParentModel, MathUtil.translationMatrix(reverseCoords));
    	
    	
    	double[] newTranslation = MathUtil.multiply(reverseParentModel, coords);
    	this.myTranslation[0] = newTranslation[0];
    	this.myTranslation[1] = newTranslation[1];


        myParent.myChildren.remove(this);
        myParent = parent;
        myParent.myChildren.add(this);
        
    }
    
    protected float[] localToAbsolute(double x, double y){
    	double[][] translation = MathUtil.translationMatrix(this.getGlobalPosition());
		double[][] rotation = MathUtil.rotationMatrix(this.getGlobalRotation());
		double[][] scale = MathUtil.scaleMatrix(this.getGlobalScale());
//		System.out.println("-----------Rotation: "+this.getGlobalRotation()+" --------");
//		for(int i = 0; i< 3; i++){
//			for(int j = 0; j < 3; j++){
//				System.out.print(rotation[j][i]+", ");
//			}
//			System.out.print("\n");
//		}
		
		
		double[] result = {x,y,1};
		result = MathUtil.multiply(scale, result);
		result = MathUtil.multiply(rotation, result);
		
		result = MathUtil.multiply(translation, result);
//		System.out.println("Original: ("+x+","+y+") New: ("+result[0]+","+result[1]+")");
		float[] floatResult = {(float)result[0],(float)result[1]};
		return floatResult;
		
    }
    

}
